# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'struct-lang'
copyright = '2023, mwegrzyn'
author = 'mwegrzyn'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'nbsphinx',
    'sphinx.ext.mathjax',
    'sphinx_gallery.load_style',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '**.ipynb_checkpoints']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_book_theme"#'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# Add to beginning of each page
# https://nbsphinx.readthedocs.io/en/0.7.0/prolog-and-epilog.html
# https://stackoverflow.com/questions/27934885/how-to-hide-code-from-cells-in-ipython-notebook-visualized-with-nbviewer/47017746#47017746
#nbsphinx_prolog = """
#.. raw:: html
#<!--
#    <style>
#        h1 {
#            color: black;
#        }
#
#    /* Refsnes Data https://www.w3schools.com/csS/css3_buttons.asp */
#    #toggleButton {
#      background-color: #4CAF50; /* Green */
#      border: none;
#      color: white;
#      padding: 5px 10px;
#      margin-left: 0px;
#      text-align: center;
#      text-decoration: none;
#      display: inline-block;
#      font-size: 14px;
#    }
#
#    </style>
#
#    <script>
#      // user3476463 https://stackoverflow.com/questions/47255288/hide-code-in-jupyter-notebook
#      function code_toggle() {
#        if (code_shown){
#          $('div.nbinput.docutils.container').hide('500');
#          $('#toggleButton').val('Show Code')
#        } else {
#          $('div.nbinput.docutils.container').show('500');
#          $('#toggleButton').val('Hide Code')
#        }
#        code_shown = !code_shown
#      }
#
#      $( document ).ready(function(){
#        code_shown=false;
#        $('div.nbinput.docutils.container').hide()
#      });
#
#      // designerdre101, Mechanical snail https://stackoverflow.com/questions/584751/inserting-html-into-a-div/4370415#4370415
#      $('ul.current.nav.sidenav_l1').append('<li><form action="javascript:code_toggle()"><input type="submit" id="toggleButton" value="Show Code"></form></li>');
#
#    </script>
#-->
#"""
