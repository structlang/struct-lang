.. struct-lang documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   The gallery function is explained here https://nbsphinx.readthedocs.io/en/0.7.0/subdir/gallery.html


Struct Lang
===========

.. nbgallery::
    :caption: Table of Contents
    :name: rst-gallery
    :glob:

    *

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
